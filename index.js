const express = require('express');
const app = express();
const port = 3000;
// response send Hello World to clients
app.get('/', (req, res) => res.send("Hello World"));
app.listen(port, () => console.log(`WebApp is listening on port ${port}!`));

